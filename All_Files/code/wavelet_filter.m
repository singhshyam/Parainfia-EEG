% rw1= load('level1.txt');
% xden = wdenoise(rwl);
% 
% plot([rwl' xden'])
% legend('Original Signal','Denoised Signal')

load wnoisydata
xden = wdenoise(wnoisydata,5,'DenoisingMethod','BlockJS');

h1 = plot(wnoisydata.t,[wnoisydata.noisydata(:,1) xden.noisydata(:,1)]);
h1(2).LineWidth = 2;
legend('Original','Denoised')
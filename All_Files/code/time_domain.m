% close all;
% clc;
% % load time domain signal and deterend the signals
% y = load('atharva2.csv');
% % x= y(:,1);
% % fs = 512;
% % t = linspace(0,length(x)/fs,length(x));
% % z = detrend(x);
% 
% 
% 
% 
% Fs = 512;                                                  % Sampling Frequency (Hz)
% Fn = Fs/2;                                                  % Nyquist Frequency (Hz)
% Wp = [ 4 45]/Fn;                                            % Passband Frequency (Normalised)
% Ws = [ 3 46]/Fn;                                            % Stopband Frequency (Normalised)
% Rp =   1;                                                   % Passband Ripple (dB)
% Rs = 150;                                                   % Stopband Ripple (dB)
% [n,Ws] = cheb2ord(Wp,Ws,Rp,Rs);                             % Filter Order
% [z,p,k] = cheby2(n,Rs,Ws);                                  % Filter Design
% [sosbp,gbp] = zp2sos(z,p,k);                                % Convert To Second-Order-Section For Stability
% figure(3)
% freqz(sosbp, 2^16, Fs)                                      % Filter Bode Plot
% filtered_signal = filtfilt(sosbp, gbp, original_signal);  
% % plot(t,z);
% % xlabel('Time(sec)');
% % ylabel('Electrical Activity');
% % title('Mindwave EEG Raw Data Plot');
% 
% 
% % 
% % % stft on deterented signal.
% % fs = 512;
% % t = 0:1/fs:60;
% % % y = sin(128*pi*t) + sin(256*pi*t); % sine of periods 64 and 128.
% % y = z;
% % level = 4;
% % windowsize = 512;
% % window = hanning(windowsize);
% % nfft = windowsize;
% % noverlap = (windowsize/2-1);
% % [S,F,T] = spectrogram(y,window,noverlap,nfft,fs);
% % imagesc(T,F,log10(abs(S)))
% % set(gca,'YDir','Normal')
% % xlabel('Time (sec)')
% % ylabel('Freq (Hz)')
% % title('Short-time Fourier Transform spectrum')
% % 
% % 



      eegdata = load('atharva1.csv');
      Fs = 512;
      Fstop1 = 0.1; % First Stopband Frequency
      Fpass1 = 0.2; % First Passband Frequency
      Fpass2 = 45;  % Second Passband Frequency
      Fstop2 = 46; % Second Stopband Frequency
      Astop1 = 60;  % First Stopband Attenuation (dB)
      Apass  = 1;   % Passband Ripple (dB)
      Astop2 = 80;  % Second Stopband Attenuation (dB)
      match  = 'stopband'; % Band to match exactly
      % Construct an FDESIGN object and call its BUTTER method.
      bpfilt = fdesign.bandpass(Fstop1,Fpass1,Fpass2,Fstop2,Astop1,Apass,Astop2,Fs);
      Hd     = design(bpfilt, 'butter', 'MatchExactly', match);
      eegdata = filter(Hd,eegdata);
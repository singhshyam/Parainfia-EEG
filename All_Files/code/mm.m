A =[1 2 3 4; 2 nan 4 5; 4 nan nan 5;];


for k = 2:size(A, 1)
  index       = isnan(A(k, :));
  A(k, index) = A(k - 1, index);
end
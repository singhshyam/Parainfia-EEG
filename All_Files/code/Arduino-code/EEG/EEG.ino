#include <Brain.h>


Brain brain(Serial1);


long delta;
long theta;
long low_alpha;
long high_alpha;
long low_beta;
long high_beta;
long low_gamma;
long high_gamma;

void setup() {
        
    Serial.begin(9600);
    while (!Serial) {}
    
    Serial1.begin(57600);

    
    
}
 
void loop() {

   
        // "signal strength, attention, meditation, delta, theta, low alpha, high alpha, low beta, high beta, low gamma, high gamma"   
        if (brain.update() && brain.readAttention() != 0 )
        {
              delta = brain.readDelta();
              theta = brain.readTheta();
              low_alpha = brain.readLowAlpha();
              high_alpha = brain.readHighAlpha();
              low_beta = brain.readLowBeta();
              high_beta = brain.readHighBeta();       
              low_gamma = brain.readLowGamma();
              high_gamma = brain.readMidGamma();

             if(delta != 0 && theta != 0 && low_alpha != 0 && high_alpha != 0 && low_beta !=0 && high_beta != 0 && low_gamma != 0 && high_gamma != 0)
             {
               Serial.println(brain.readCSV());
             }   
        }

       

       
} 

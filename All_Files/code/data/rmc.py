
# importing pandas module 
import pandas as pd 
import csv
  
# making data frame from csv file 
data = pd.read_csv("id39.csv") 
  
# dropping passed columns 
data.drop(["TimeDate"], axis = 1, inplace = True) 
  
# display 
data

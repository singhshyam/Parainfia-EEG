from pymongo import MongoClient
client = MongoClient('localhost', 27017)
db = client['parainfia']

collection_enjoyment = db['enjoyment']
collection_video = db['video']

def recommend(user_id):
	query = {"user_id": user_id}
	ls = []
	st = set([])
	for x in collection_enjoyment.find(query):
		if x['enjoyment'] >= 3:
			for x in collection_video.find({"_id":x['video_id']},{"_id":0,"category":1}):
				ls.append(x['category'])
			st = set(ls)
	#print(ls)
	return st

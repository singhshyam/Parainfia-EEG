from NeuroPy import NeuroPy
import keyboard
from time import sleep
from scipy import signal
from scipy.signal import butter, lfilter
import pandas as pd
import numpy as np
import numpy as np
from sklearn import preprocessing
import csv
import glob
import math
import random
import load


fs = 512.0
lowcut = 3.0
highcut = 45.0

raw= []
count = 0


def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y


def predict(lst):
    a = []
    sum = 0

   
    
    Y = signal.detrend(lst)
    Z = butter_bandpass_filter(Y, lowcut, highcut, fs, order=3)
    f, t, Zxx = signal.stft(Z, fs, nperseg=256)
    #print(Zxx)

    for i in range(len(np.abs(Zxx))):
        sum = 0
        for j in range(len(np.abs(Zxx)[0])):
            sum = sum + math.log(np.abs(Zxx)[i][j] * np.abs(Zxx)[i][j])
        a.append(sum/len(np.abs(Zxx)[0]))
    
   

    enjoyment = a[2]*0.544702 - a[5]* 0.591906 + a[32]*0.104172 - a[76]* 0.482825 - a[77]* 0.654118
    
    return enjoyment

def recording(flag,neuropy):
    
   
    
    
    for x in range(20000):
        k = neuropy.rawValue
        if flag:
            raw.append(k)
        else:
            break
        sleep(0.002)

    return predict(raw)    

        
    #print(len(raw))
    


def reset():
    raw = []



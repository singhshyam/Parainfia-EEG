from pymongo import MongoClient


client = MongoClient('localhost', 27017)
db = client['parainfia']

collection_user = db['user']
collection_video = db['video']
collection_enjoyment = db['enjoyment']

def insert_user(user,password):
	i = collection_user.find().count()+1
	dict = {"_id":i,"user":user , "password":password}
	result = collection_user.insert_one(dict)
	

def find_user(user,password):
	query = {"user":user , "password":password}
	if collection_user.find(query).count() == 0:
		return False
	else:
		return True
def find_user_id(user):
	query = {"user":user}	
	for x in collection_user.find(query):
		return x['_id']

def find_video_id(path):
	query = {"path":path}	
	for x in collection_video.find(query):
		return x['_id']







def drop():
	#collection_user.drop()
	collection_enjoyment.drop()
	collection_video.drop()
	print(db.list_collection_names())

def insert_video():
	ls = [
    { "_id": 1, "category":"comedy" , "path": 'static/video/comedy/1.mp4'},
    { "_id": 2, "category":"comedy", "path": 'static/video/comedy/2.mp4'},
    { "_id": 3, "category":"mix", "path": 'static/video/mix/3.mp4'},
    { "_id": 4, "category":"mix", "path": 'static/video/mix/4.mp4'},
    { "_id": 5, "category":"mix", "path": 'static/video/mix/5.mp4'},
    { "_id": 6, "category":"news", "path": 'static/video/news/6.mp4'},
    { "_id": 7, "category":"news", "path": 'static/video/news/7.mp4'},
    { "_id": 8, "category":"news", "path": 'static/video/news/8.mp4'},
    { "_id": 9, "category":"sports", "path": 'static/video/sports/9.mp4'},
    { "_id": 10, "category":"sports", "path": 'static/video/sports/10.mp4'},
    { "_id": 11, "category":"sports", "path": 'static/video/sports/11.mp4'},
    { "_id": 12, "category":"sports", "path": 'static/video/sports/12.mp4'},
    { "_id": 13, "category":"technical", "path": 'static/video/technical/13.mp4'},
    { "_id": 14, "category":"technical", "path": 'static/video/technical/14.mp4'},
    { "_id": 15, "category":"technical", "path": 'static/video/technical/15.mp4'},
    { "_id": 16, "category":"trailer", "path": 'static/video/trailer/16.mp4'},
    { "_id": 17, "category":"trailer", "path": 'static/video/trailer/17.mp4'},

    ]
	res = collection_video.insert_many(ls)
	print(res.inserted_ids)

def insert_enjoyment(user_id,path,enjoyment):
	query = {"user_id":user_id , "video_id":find_video_id(path)}

	if collection_enjoyment.find(query).count() == 0:
		
		dict = {"user_id":user_id,"video_id":find_video_id(path) , "enjoyment":enjoyment}
		collection_enjoyment.insert_one(dict)
	else:
		
		newvalues = { "$set": { "enjoyment": enjoyment } }
		result = collection_enjoyment.update_one(query, newvalues)


'''
for x in collection_video.find():
	print(x)
'''


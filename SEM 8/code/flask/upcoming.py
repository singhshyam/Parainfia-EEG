from pymongo import MongoClient
client = MongoClient('localhost', 27017)
db = client['parainfia']
collection_enjoyment = db['enjoyment']
collection_video = db['video']

def get_upcoming(user_id):
	ls = []
	query = {"user_id":user_id}
	for x in collection_enjoyment.find(query):
		if x['enjoyment'] >= 3:
			ls.append(get_video_path(x['video_id']))
	return ls
	

def get_video_path(video_id):
	query = {"_id":video_id}
	for x in collection_video.find(query):
		return x['path']


import pickle
from sklearn import preprocessing

filename = 'finalized_model.sav'

def pred(X_test):
	min_max_scaler = preprocessing.MinMaxScaler()
	x = min_max_scaler.fit_transform(X_test)
	loaded_model = pickle.load(open(filename, 'rb'))
	result = loaded_model.predict(x)
	print(result) 
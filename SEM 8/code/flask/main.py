from flask import Flask,send_file,Response
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for
from flask import session
from imutils import face_utils
from flask import g
import imutils
import cv2
import database
import category
import display_category
import recommend
import os
from NeuroPy import NeuroPy
from time import sleep
import headset
import upcoming


neuropy = NeuroPy("/dev/tty.MindWaveMobile-DevA", 57600)

count = 0
neuro =True;
flag = True
ls = set([])
raw = []
enjoyment = 0
ls_enj = []

app = Flask(__name__)
app.secret_key = os.urandom(24)

@app.before_request
def before_request():
	global flag
	flag = True
	g.user = None
	if 'user' in session:
		g.user = session['user']


@app.route('/',methods=['post', 'get'])
def register():
	message = ""
	
	if request.method == 'POST':	
		username = request.form.get('username')
		password_1 = request.form.get('password_1')
		password_2 = request.form.get('password_2')
		
		
		if password_1 != password_2:
		 	message = "Passwords do not match"
		 	return render_template('register.html',message  = message)
		else:
			database.insert_user(username,password_1)
			return redirect(url_for('login'))
		

	return render_template('register.html',message  = message)

@app.route('/login',methods=['post', 'get'])
def login():
	message = ""
	global ls_enj
	if request.method == 'POST':

		session.pop('user', None)
		session.pop('user_id', None)
		session.pop('video', None)
		session.pop('enjoyment',None)
		ls_enj = []
		
		username = request.form.get('username')
		password = request.form.get('password')
		
		if(database.find_user(username,password)):
			session['user'] = request.form.get('username')
			session['user_id'] = database.find_user_id(request.form.get('username'))
			return redirect(url_for('stream'))
		else:
			message = "Invalid username or password"
			return render_template('login.html',message = message)

	return render_template('login.html')

#video streaming
@app.route('/stream',methods=['post', 'get'])
def stream():
	if g.user:
		global enjoyment
		path = display_category.display_initial()
		rec = recommend.recommend(session['user_id']) #recommendation data
		catg = category.get_category()
		if request.method == 'POST':
			if request.form.get('category'):
				path = display_category.display_category(request.form.get('category'))
				print(path)
		upcome = upcoming.get_upcoming(session['user_id'])
		return render_template('index.html',path = path,recommend = rec,catg = catg,username = session['user'],upcome = upcome)

	return redirect(url_for('login'))

#eeg recording
@app.route('/eeg',methods=['post', 'get'])
def eeg():
	print("reading")
	global flag
	if request.method == 'POST':
		data = request.form.to_dict()
		temp = data['status']
		
		if temp == "True":
			if flag:
				neuropy.start()
				flag = False
			enjoyment = headset.recording(True,neuropy)
			print(str(enjoyment))
			neuropy.stop()
			database.insert_enjoyment(session['user_id'],data['p'],enjoyment)
			
	return str(enjoyment)

@app.route('/video_feed',methods=['post', 'get'])
def video_feed():
	return Response(gen(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/nxt',methods=['post', 'get'])
def nxt():
	return upcoming.next(session['user_id'])

def gen(): 
	vid = cv2.VideoCapture(0)   
	while True:
		_, frame = vid.read()
		frame = cv2.flip(frame, 1)
		frame = imutils.resize(frame, width=200, height=200)
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

		cv2.imwrite('t.jpg', frame)
		yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + open('t.jpg', 'rb').read() + b'\r\n')
        

if __name__ == "__main__":
	app.run(debug = True)

from pymongo import MongoClient
import random

client = MongoClient('localhost', 27017)
db = client['parainfia']
collection_video = db['video']

def display_category(category):
	ls = []
	query = {"category": category}
	for x in collection_video.find(query):
		ls.append(x['path'])
	print(ls)
	return random.choice(ls)

def display_initial():
	ls = []
	for x in collection_video.find():
		ls.append(x['path'])
	return random.choice(ls)
csvfiles = 64;
for m=1:csvfiles
    
filename = strcat("./data/id",sprintf("%d.csv",m));
disp(filename);
xd = load(filename);
dn = wdenoise(xd,4);

fs = 512;
t = 0:1/fs:60;

z = bandpass(dn,[03, 45],512);
level = 4;
windowsize = 512;
window = hanning(windowsize);
nfft = windowsize;
noverlap = (windowsize/2-1);
[S,F,T] = spectrogram(z,window,noverlap,nfft,fs);

data =abs(S);
mx = mean(data,1);
disp(mx);
% csvwrite('featureMatrix.csv',mx)
dlmwrite('step.csv',mx,'delimiter',',','-append');
end
imagesc(T,F,log10(abs(S)))
set(gca,'YDir','Normal')
xlabel('Time (sec)')
ylabel('Freq (Hz)')
title('Short-time Fourier Transform spectrum')

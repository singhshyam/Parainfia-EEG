clc; 
clear;
close all;

%stft
id = 64;
for m=1:2
    
	filename = strcat("./data/id",sprintf("%d.csv",m));
	disp(filename);
	xd = load(filename);

	dt = detrend(xd);
	dn = wdenoise(dt,4);
	z = bandpass(dn,[03, 45],512);

	fs = 512;
	t = 0:1/fs:60;

	level = 4;
	windowsize = 512;
	window = hamming(windowsize);
	nfft = windowsize;
	noverlap = (windowsize/2-1);
	[S,F,T] = spectrogram(z,window,noverlap,nfft,fs);

	data =abs(S);
	mx = mean(data,1);
	disp(mx);
	% csvwrite('featureMatrix.csv',mx)
	dlmwrite('full.csv',data,'delimiter',',','-append');
end

imagesc(T,F,log10(abs(S)))
set(gca,'YDir','Normal')
xlabel('Time (sec)')
ylabel('Freq (Hz)')
title('Short-time Fourier Transform spectrum')

%stepwise regression
fm = load('featurematrix.csv');
X =fm(2:65,1:114);
X_norm = (X-min(X))./(max(X)-min(X));
y = fm(2:65,115);

epoch = 1;
for i=1:epoch
mdl = stepwiselm(X_norm,y,'linear','PEnter',0.05,'PRemove',0.10);
end

%Selection of the dimensional reduction matrix 
[num , txt,  raw] = xlsread('featurematrix.csv');
data = cell2mat(raw(2:end, [53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 95 96 97 98 99 100 101 102 103 104 105 106 108 109 110 111 112 113 114]));
addRow = raw(2:end,94:95);
B = cell2mat(addRow);
p=(B(:,1).*B(:,2));
p1 = round(p,3);
xdata = [data p1];
csvwrite('test.csv' ,xdata);


%multivariate regression

Sm = load('stepwise.csv');
X_pre = Sm(:,1:61);
m = size(X_pre,1);
X_pre = [ones(m, 1) X_pre];

y_res = Sm(:,62);


%b = mvregress(X_pre, y_res);
%b = mvregress(X_pre,y_res,'algorithm','cwls');

[b,Sigma,E,CovB,logL] = mvregress(X_pre,y_res,'algorithm','cwls');

ptb = bsxfun(@times,X_pre,b'); 
ptb = sum(ptb,2);
Y = (ptb);




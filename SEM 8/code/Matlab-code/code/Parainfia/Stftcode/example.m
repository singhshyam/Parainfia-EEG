clear, clc, close all

x = load('level1.txt');
win = 512;
hop = 256;
nfft = 512*4;
fs = 512;

dt = detrend(x);
dn = wdenoise(dt,4);
z = bandpass(dn,[3, 45],512);
    
[STFT, f, t] = stft(z, win, hop, nfft, fs);
disp(round(STFT));
plot(STFT);
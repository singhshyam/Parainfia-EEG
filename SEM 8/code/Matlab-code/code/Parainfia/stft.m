clc; 
clear;
close all;

id = 64;
for m=1:64
    
	filename = strcat("./data/id",sprintf("%d.csv",m));
	disp(filename);
	xd = load(filename);

	dt = detrend(xd);
	dn = wdenoise(dt,4);
	z = bandpass(dn,[03, 45],512);

	fs = 512;
	t = 0:1/fs:60;

	level = 4;
	windowsize = 512;
	window = hamming(windowsize);
	nfft = windowsize;
	noverlap = (windowsize/2-1);
	[S,F,T] = spectrogram(z,window,noverlap,nfft,fs);

	data =log10(abs(S));
	mx = mean(data,1);
    
%     dtt = abs(data(3:46,1:114));
    
    disp(mx);
	% csvwrite('featureMatrix.csv',mx)
    dlmwrite('xx.csv',mx,'delimiter',',','-append');
end

% imagesc(T,F,log10(abs(S)))
% set(gca,'YDir','Normal')
% xlabel('Time (sec)')
% ylabel('Freq (Hz)')
% title('Short-time Fourier Transform spectrum')




clc; 
clear;
close all;

Sm = load('train.csv');
X_pre = Sm(:,1:14);
m = size(X_pre,1);


X_norm = (X_pre-min(X_pre))./(max(X_pre)-min(X_pre));
X_pre = [ones(m, 1) X_norm];
y_res = Sm(:,15);
% y_res = load('y_res.csv');


b = mvregress(X_pre, y_res);
%b = mvregress(X_pre,y_res,'algorithm','cwls');
% [b,Sigma,E,CovB,logL] = mvregress(X_pre,y_res,'algorithm','cwls');

ptb = bsxfun(@times,X_pre,b'); 
ptb = sum(ptb,2);
Y = (ptb);

for i=1:64
    result = round(abs(Y-y_res));
end


test1 = load('test.csv');
[n , d] = size(test1);
X_test =test1(:,1:14);

X_norm =(X_test-min(X_test))./(max(X_test)-min(X_test));

X_test= [ones(n, 1) X_test];
% 
% 
ptb1 = bsxfun(@times,X_test,b');
ptb1 = sum(ptb1,2);
Y_test = (ptb1);
disp(round((Y_test)));
% disp(Y_test);




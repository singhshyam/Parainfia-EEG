clear, clc, close all

id =64;
for m = 65:67
	filename = strcat("../data/id",sprintf("%d.csv",m));
	disp(filename);
	xd = load(filename);
    fs = 512;
    dt = detrend(xd);
    dn = wdenoise(dt,4);
    x = bandpass(dn,[03, 45],512);
    xmax = max(abs(x));                 
    x = x/xmax;                         

    %define analysis parameters
    xlen = length(x);                   
    wlen = 512;                       
    h = wlen/2;                        
    nfft = 512;                       

    % define the coherent amplification of the window
    K = sum(hamming(wlen, 'periodic'))/wlen;

    % perform STFT
    [s, f, t] = stft(x, wlen, h, nfft, fs);

    % take the amplitude of fft(x) and scale it, so not to be a
    % function of the length of the window and its coherent amplification
    s = abs(s)/wlen/K;


    % convert amplitude spectrum to dB (min = -120 dB)
    % s = 20*log10(s + 1e-6);
     s =log10(abs(s));

     requiredData = abs(s(3:46,:));
     dlmwrite('testsummet.csv',requiredData,'delimiter',',','-append');
 
end










% % plot the spectrogram
% figure(1)
% imagesc(t, f, s)
% set(gca,'YDir','normal')
% set(gca, 'FontName', 'Times New Roman', 'FontSize', 14)
% xlabel('Time, s')
% ylabel('Frequency, Hz')
% title('Amplitude spectrogram of the signal')
% 
% handl = colorbar;
% set(handl, 'FontName', 'Times New Roman', 'FontSize', 14)
% ylabel(handl, 'Magnitude, dB')
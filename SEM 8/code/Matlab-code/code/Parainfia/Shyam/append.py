with open('ms.csv') as data:
	rows = data.read().strip().split()
	modified_rows = []
	with open('labels.csv') as labeldata:
		labels = labeldata.read().strip().split()
		for ind,label in zip(range(0,len(rows),44),labels):
			for row in rows[ind:ind+44]:
				modified_rows.append(','.join([row,label]) + '\n')
	with open('labelled_data.csv','w') as labelled_data:
		labelled_data.writelines(modified_rows)




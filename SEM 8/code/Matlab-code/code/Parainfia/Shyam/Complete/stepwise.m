clc; 
clear;
close all;

fm = load('data.csv');
y = load('labels.csv');
X =fm(:,1:115);
[n,d] = size(X);
X_norm = (X-min(X))./(max(X)-min(X));
X_norm = [ones(n,1) X_norm];

%stepwise-Regression Model for feature Selection.

Linear_Model = stepwiselm(X_norm,y,'linear','PEnter',0.05,'PRemove',0.10);



from scipy import signal
from scipy.signal import butter, lfilter
import pandas as pd
import numpy as np
import csv


filename =pd.read_csv ('data/id15.csv')
print(len(filename))


fs = 512.0
lowcut = 3.0
highcut = 45.0

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def predict(lst):
    a = []
    sum = 0

    Y = signal.detrend(lst)
    Z = butter_bandpass_filter(Y, lowcut, highcut, fs, order=3)
    f, t, Zxx = signal.stft(Z, fs=512, window='hamm', nfft=512, detrend=False, return_onesided=True, boundary='zeros', padded=True, axis=-1)

    # print(len(np.abs(Zxx)))
   

    for i in range(len(np.abs(Zxx))):
        sum = 0
        for j in range(len(np.abs(Zxx)[0])):
            sum = sum + np.log10(np.abs(Zxx)[i][j])
        a.append(sum/len(np.abs(Zxx)[0]))
    
   

    # enjoyment = a[2]*0.544702 - a[5]* 0.591906 + a[32]*0.104172 - a[76]* 0.482825 - a[77]* 0.654118
    
    # return enjoyment

predict(filename)    
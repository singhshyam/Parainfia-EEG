from scipy import signal
from scipy.fftpack import fft, fftshift
from scipy.signal import hamming 
import matplotlib.pyplot as plt
import numpy as np
import csv


def single_spectrogram(inseq,fs,wlen,h,imag=False):
   
    NFFT = int(2**(np.ceil(np.log2(wlen)))) 
    K = np.sum(np.hamming(wlen))/wlen
    raw_data = inseq
    raw_data = raw_data/np.amax(np.absolute(raw_data))
    stft_data,_,_ = STFT(raw_data,wlen,h,NFFT,fs)
    s = np.absolute(stft_data)/wlen/K
    if np.fmod(NFFT,2):
        s[1:,:] *=2
    else:
        s[1:-2] *=2        
    real_data = np.transpose(20*np.log10(s + 10**-6))
    if imag:
        imag_data = np.angle(stft_data).astype(np.float32)
        return real_data,imag_data 
    print(real_data)    
    return real_data


x = 'data/id15.csv'
wlen = 512
h = 256
fs = 512




import numpy as np 


A = np.arange(9).reshape(3,3)
X = np.array([A[:], A[:]*2, A[:]*3])

Y = X[:]

a = X @ Y
print(a)

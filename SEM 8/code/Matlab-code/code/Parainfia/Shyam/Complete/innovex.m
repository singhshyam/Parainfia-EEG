 clear, clc, close all

% %user Stft calculation
%     id = 64;
%     for m =15:64
% 	filename = strcat("./data/id",sprintf("%d.csv",m));
% 	xd = load(filename);
%     fs = 512;
%     dt = detrend(xd);
%     dn = wdenoise(dt,4);
%     x = bandpass(dn,[03, 45],512);
% 
%     xmax = max(abs(x));                 
%     x = x/xmax;                         
% 
% %     define analysis parameters
%     xlen = length(x);                   
%     wlen = 512;                       
%     h = wlen/2;                        
%     nfft = 512;                       
% 
%     K = sum(hamming(wlen, 'periodic'))/wlen;
% 
% %     perform STFT
%     [s, f, t] = stft(x, wlen, h, nfft, fs);
% 
%     s = abs(s)/wlen/K;
%     s =log10(abs(s));
% 
%     requiredData = abs(s(3:46,:));
%     requiredData = mean(requiredData);
%     dlmwrite('data.csv',requiredData,'delimiter',',','-append');
%     end

% multivariate Regression Model
% 
dataset = load('labelled_data.csv');
pred = dataset(:,1:115);
target = dataset(:,116);
[n,d] = size(pred);
X_pred =(pred-min(pred))./(max(pred)-min(pred));
X_pred = [ones(n,1) X_pred];



[beta,sigma,E,V] = mvregress(X_pred,target);

se = sum(sqrt(diag(V)));

% Error = bsxfun(@sub,beta,se); 

ptb = bsxfun(@times,X_pred,beta'); 
ptb = sum(ptb,2);
Y = ptb;

y_new = cell(50,1);
for i=1:50
    ind = (i-1)*44+(1:44);
    y_new{i,1} = round(mean(Y(ind,:)));
end


save model.vec beta V sigma E


tg = load('../labels.csv');
disp("accuracy: target "+tg+" Predicted: " + y_new);

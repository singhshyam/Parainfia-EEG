clear;
clc;
close all;

id = 67;
    for m= 15
	filename = strcat("./data/id",sprintf("%d.csv",m));
	xd = load(filename);
    fs = 512;
    dt = detrend(xd);
    dn = wdenoise(dt,4);
    x = bandpass(dn,[03, 45],512);

    xmax = max(abs(x));                 
    x = x/xmax;                         

%     define analysis parameters
    xlen = length(x);                   
    wlen = 512;                       
    h = wlen/2;                        
    nfft = 512;                       

    K = sum(hamming(wlen, 'periodic'))/wlen;

%     perform STFT
    [s, f, t] = stft(x, wlen, h, nfft, fs);

    s = abs(s)/wlen/K;
    s =log10(abs(s));

    requiredData = abs(s(3:46,:));

    
    end
    %dlmwrite('dataset.csv',requiredData,'delimiter',',','-append');
 
    pred = requiredData(:,1:115);
	[n,d] = size(pred);
	X_pred =(pred-min(pred))./(max(pred)-min(pred));
	X_pred = [ones(n,1) X_pred];

	model_weights = matfile('model.vec');
	beta = model_weights.beta;
	V  = model_weights.V;
	se = sqrt(diag(V)); 

	ptb = bsxfun(@times,X_pred,beta'); 
	ptb = sum(ptb,2);
	pred_target = ptb;
    
        i =1;
        ind = (i-1)*44+(1:44);
        y_new = round(mean(pred_target(ind,:)));
        disp("Enjoyment level of user for this Video: "+y_new);
   	


clear;
clc;
close all;

dataset = load('labelled_data.csv');
pred = dataset(:,1:115);
target = dataset(:,116);
[n,d] = size(pred);
X_pred =(pred-min(pred))./(max(pred)-min(pred));
X_pred = [ones(n,1) X_pred];


pred_cell = cell(50,1);
target_cell = cell(50,1);
for i=1:50
    ind = (i-1)*44+(1:44);
    pred_cell{i,1} = [ones(44,1) pred(ind,:)];
    target_cell{i,1} =target(ind,:);
end
% 
% Split into Training and Test Set 70 - 30 split  (70 - training , 30 - test )
% training_size = floor(size(pred_cell,1)*.70);
% training_features = pred_cell(1:training_size,:);
% training_target = target_cell(1:training_size,:);
% 
% % Test set (30 - test)
% test_size = size(pred_cell,1)-training_size;
% test_set = pred_cell(training_size:training_size+test_size,:);
% test_target = target_cell(training_size:training_size+test_size,:);


beta = mvregress(pred,target);


% % predictor = load('ms.csv');
% % target = load('labels.csv');
% 
% % pred_cell = cell(50,1);
% % for i=1:50
% %     ind = (i-1)*44+(1:44);
% %     pred_cell{i,1} = [ones(44,1) predictor(ind,:)];
% % end
% 
% %b = mvregress(pred_cell, target);
% 
% % [b,Sigma,E,CovB,logL] = mvregress(pred_cell,target,'algorithm','cwls');
% 
% 
% 
% % Split into Training and Test Set
% % 70 - 30 split  (70 - training , 30 - test )
% training_size = floor(size(features,1)*.70);
% training_features = features(1:training_size,:);
% training_ground = y_ground(1:training_size,:);
% 
% % Test set (30 - test)
% test_size = size(features,1)-training_size;
% test_set = features(training_size:training_size+test_size,:);
% test_ground = y_ground(training_size:training_size+test_size,:);
% 
% dataset = load('labelled_data.csv');
% pred = dataset(:,1:115);
% target = dataset(:,116);
% [n,d] = size(pred);
% X_pred =(pred-min(pred))./(max(pred)-min(pred));
% X_pred = [ones(n,1) X_pred];
% 
% [beta,sigma,E,V] = mvregress(X_pred,target);
% 
% se = sqrt(diag(V));
% 
% ptb = bsxfun(@times,X_pred,beta'); 
% ptb = sum(ptb,2);
% Y = (ptb);
% % Y = Add(Y,se);
% % for i=1:n
% % result =abs(target-Y); 
% % disp(result);
% % end
% 
% new_y =cell(50,1);
% for i=1:50
%     ind = (i-1)*44+(1:44);
%     new_y{i,1} =round(mean(Y(ind,:)));
%     dlmwrite('output.csv',new_y{i,1},'delimiter',',','-append');
% end
% 
% tg = load('labels.csv');
% disp("accuracy: target "+tg+" Predicted: " + new_y);
% 
% X_test = load('testsummet.csv');
% [n,d] = size(X_test);
% X_test =(X_test-min(X_test))./(max(X_test)-min(X_test));
% X_test = [ones(n,1) X_test];
% ptb = bsxfun(@times,X_test,beta'); 
% ptb = sum(ptb,2);
% Y_test = (ptb);
% 
% newtest_y =cell(3,1);
% for i=1:3
%     ind = (i-1)*44+(1:44);
%     newtest_y{i,1} = round(mean(Y(ind,:)));
% end
% 
% 

# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 20:16:54 2019

@author: Dell
"""

import pandas as pd  
import numpy as np  
import matplotlib.pyplot as plt  
#matplotlib inline

dataset = pd.read_csv('labelled_data.csv')  
dataset.shape

dataset.describe()

#X = dataset[['Petrol_tax', 'Average_income', 'Paved_Highways','Population_Driver_licence(%)']]
#y = dataset['Petrol_Consumption'] 
X = dataset.iloc[:,0:115].values
y = dataset.iloc[:,115].values

from sklearn.model_selection import train_test_split  
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.05, random_state=0)  

from sklearn.linear_model import LinearRegression  
regressor = LinearRegression()  
regressor.fit(X_train, y_train) 
 

#coeff_df = pd.DataFrame(regressor.coef_, X.columns, columns=['Coefficient'])  

y_pred = regressor.predict(X_test) 

df = pd.DataFrame({'Actual': y_test, 'Predicted': y_pred})  
print(df)

from sklearn.metrics import accuracy_score
from sklearn.metrics import r2_score
acc = r2_score(y_test, y_pred)
print(acc*100,'%')

accuracy = regressor.score(X_test,y_test)
#print(accuracy*100,'%')

from sklearn import metrics  
print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))  
print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))  
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))  

'''
dataset.plot(x='', y='Scores', style='o')  
plt.title('Hours vs Percentage')  
plt.xlabel('Hours Studied')  
plt.ylabel('Percentage Score')  
plt.show() '''
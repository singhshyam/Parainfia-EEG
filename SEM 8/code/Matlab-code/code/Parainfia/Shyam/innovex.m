 clear, clc, close all

%user Stft calculation
    m=67;
	filename = strcat("../data/id",sprintf("%d.csv",m));
	xd = load(filename);
    fs = 512;
    dt = detrend(xd);
    dn = wdenoise(dt,4);
    x = bandpass(dn,[03, 45],512);

    xmax = max(abs(x));                 
    x = x/xmax;                         

    define analysis parameters
    xlen = length(x);                   
    wlen = 512;                       
    h = wlen/2;                        
    nfft = 512;                       

    K = sum(hamming(wlen, 'periodic'))/wlen;

    perform STFT
    [s, f, t] = stft(x, wlen, h, nfft, fs);

    s = abs(s)/wlen/K;
    s =log10(abs(s));

    requiredData = abs(s(3:46,:));
    dlmwrite('username1.csv',requiredData,'delimiter',',','-append');
stft calculation end

user data feature Selection.
    
    [num , txt,  raw] = xlsread('featurematrix.csv');
    data = cell2mat(raw(2:end, [53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 95 96 97 98 99 100 101 102 103 104 105 106 108 109 110 111 112 113 114]));
    addRow = raw(2:end,94:95);
    B = cell2mat(addRow);
    p=(B(:,1).*B(:,2));
    p1 = round(p,3);
    xdata = [data p1];
    csvwrite('test.csv' ,xdata);

multivariate Regression Model

dataset = load('labelled_data.csv');
pred = dataset(:,1:115);
target = dataset(:,116);
[n,d] = size(pred);
X_pred =(pred-min(pred))./(max(pred)-min(pred));
X_pred = [ones(n,1) X_pred];

[beta,sigma,E,V] = mvregress(X_pred,target);

se = diag(V);
ptb = bsxfun(@times,X_pred,beta'); 
ptb = sum(ptb,2);
Y = (ptb);
Y = Add(Y,se);
for i=1:n
result =abs(target-Y); 
disp(result);
end
save model.vec beta V
model_weights = matfile('model.vec');
beta = model_weights.beta;
new_y =cell(50,1);
for i=1:50
    ind = (i-1)*44+(1:44);
    new_y{i,1} = round(mean(Y(ind,:)));
end

tg = load('labels.csv');
disp("accuracy: target "+tg+" Predicted: " + new_y);

X_test = load('username1.csv');
[n,d] = size(X_test);
X_test =(X_test-min(X_test))./(max(X_test)-min(X_test));
X_test = [ones(n,1) X_test];
ptb = bsxfun(@times,X_test,beta'); 
ptb = sum(ptb,2);
Y_test = (ptb);

newtest_y =cell(3,1);
for i=1:3
    ind = (i-1)*44+(1:44);
    newtest_y{i,1} = round(mean(Y(ind,:)));
end
disp(newtest_y);

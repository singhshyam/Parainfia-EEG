from sklearn.linear_model import LinearRegression
# from sklearn.datasets import load_boston
from mlxtend.feature_selection import ExhaustiveFeatureSelector as EFS
import pandas as pd
import numpy as np

dataset = pd.read_csv('data.csv');
X= dataset.iloc[:, :1:115]
y = dataset.iloc[:, 116]


lr = LinearRegression()

efs = EFS(lr,min_features=4,max_features=5, scoring='neg_mean_squared_error',cv=10)

efs.fit(X, y)

print('Best MSE score: %.2f' % efs.best_score_ * (-1))
print('Best subset:', efs.best_idx_)
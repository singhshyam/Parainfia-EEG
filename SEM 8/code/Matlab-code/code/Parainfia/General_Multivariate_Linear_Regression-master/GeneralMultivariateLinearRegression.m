%%  General Multivariate Linear Regression
%%  Initialization

clear;
close all;
clc;
fprintf('Initialized\n');

%%  Load Data

train_data_address = 'train.csv';
test_data_address = 'test.csv';

[rw,cl]=size(train_data_address);
n = size(test_data_address,1);

train_data = load(train_data_address);
X = [ones(333, 1), train_data(:, 2:(size(train_data, 2)) - 1)];
y = train_data(:, size(train_data, 2));

fprintf('Data loaded\n');

%%  Feature Scaling

[X, mu, stddev] = featureScaling(X);

fprintf('Feature scaled\n');

%%  Gradient Descent

%   Set learning rate(alpha) and epoch
alpha = 0.01;
epoch = 10;

%   Initialize theta and Run Gradient Descent
theta = zeros(size(X, 2), 1);
[theta, costHistory] = gradientDescent(X, y, theta, alpha, epoch);

fprintf('Computed Gradient Descent\n');

%%  Plot graphs, hypothesis and display Theta

%   Plot graph Cost - Num of Iterations
figure('Name', 'Cost - Num of Iterations');
plot(1:numel(costHistory), costHistory, '-b', 'LineWidth', 2);
xlabel('Num of Iterations');
ylabel('Cost');

%   Plot all graph Prediction & Label y - Feature x
% featureNum = 0;
% for i = 2:size(X, 2)
%     featureNum = i - 1;
%     figure('Name', strcat('Prediction & Label y - Feature x', int2str(featureNum)));
%     plot(X(:, i), y, 'rx', 'MarkerSize', 10);
%     ylabel('Prediction & Label y');
%     xlabel(strcat('Feature x', int2str(featureNum)));
%     hold on;
%     plot(X(:, i), X * theta, '-');
%     legend('Training Data', 'Linear Regression');
%     hold off;
% end

%   Display results
fprintf('Theta computed from Gradient Descent:\n\n');
fprintf('%f\n', theta);
fprintf('\n');

%%  Compare to testcase

%   Load test data
testcase = load(test_data_address);
X_Testcase = [ones(173, 1), testcase(:, 2:(size(testcase, 2) - 1))];
y_Testcase = testcase(:, size(testcase, 2));

%   Scale test data
for i = 2:size(X_Testcase, 2)
    X_Testcase(:, i) = (X_Testcase(:, i) - mu(i - 1)) / stddev(i - 1);
end

%   Compute the result and Mean Absolute Error
test_result = X_Testcase * theta;
MeanAbsoluteError = abs(test_result - y_Testcase);
totalMeanAbsoluteError = sum(MeanAbsoluteError) / length(y_Testcase);
fprintf('Tested by testcase:\n');
fprintf('Mean Absolute Error: %f\n\n', totalMeanAbsoluteError);

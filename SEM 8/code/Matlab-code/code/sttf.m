% First, we load the dataset, containing:
% @ 'predictors' is [10 x 6] containing 10 observations on 6 predictors (6
% 	different independent variables).
% @ 'target' is a [10 x 3] matrix containing the three-dimensional vector of
%   targets (dependent variable) for each of the 10 observations.
load dataset.mat 


% Our problem statement:
% We are trying to find a mapping or function that will fit our
% (predictor,target) data using multivariate regression. The goal 
% is to find a vector [B w1 w2 � w6] that satisfies a least-squares 
% fit to Y as follows:
%
% Y(1) = B(1) + w1*V1(1) + w2*V2(1) + ... + w6*V6(1)
% Y(2) = B(2) + w1*V1(1) + w2*V2(1) + ... + w6*V6(1)
% Y(3) = B(3) + w1*V1(1) + w2*V2(1) + ... + w6*V6(1)
%
% In the above equations, [w1 w2 .. w6] are six scalar weights and B is a
% [1 x 3] vector containing the intercepts for each of the three dimensions
% of Y.
% 
% [V1 V2 .. V6] are the columns of the preditor matrix. So V1 is a [10 x 1]
% vector and V1(1) is the first of 10 observations. 

% Because our target data is multidimensional, we need to first put our 
% predictor data into a [10 x 1] cell array, one cell per observation. Each
% cell contains a [3 x 9] matrix containing the intercepts and independent
% variables for that observation. There is one row per dimension in Y.


pred_cell = cell(10,1);
for i = 1:10
    % For each of the n points, set up a design matrix specifying
    % a different intercept but common slope terms
    pred_cell{i,1} = [eye(3), repmat(predictors(i,:),3,1)];
end


% The result from passing our preditors and targets into MVREGRESS using
% the cell format is a vector 'b' of weights. It is [9 x 1] because there
% are 3 weights (one for every intercept for each dimension of the target)
% plus 6 weights (one for each independent variable).
b = mvregress(pred_cell, target);

%Calculate the Y values from above
ptb = bsxfun(@times,predictors,b(4:9)'); %predictors times b values for independent variables
ptb = sum(ptb,2); %sum columnwise to get the value
Y   = bsxfun(@plus,b(1:3)',ptb); %add to intercepts




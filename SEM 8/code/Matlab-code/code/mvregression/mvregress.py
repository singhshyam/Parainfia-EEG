import matplotlib.pyplot as plt
import csv
from textblob import TextBlob
import pandas
import sklearn
import pickle
import numpy as np
from sklearn import preprocessing
from sklearn import cross_validation as cv
from sklearn.metrics import explained_variance_score, mean_squared_error,r2_score 
from sklearn.pipeline import Pipeline
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import cross_val_score, train_test_split 
from sklearn.learning_curve import learning_curve
from sklearn.externals import joblib
from sklearn.linear_model import LinearRegression

#load data and plot data
labels=["LIST PRICE","BEDS","BATHS","SQFT","LOT SIZE","YEAR BUILT",
        "PARKING SPOTS","PARKING TYPE","ORIGINAL LIST PRICE","LAST SALE PRICE"]
houses=pandas.read_csv('./data/redfin_2015-02-17_94555_results_massaged.csv',quoting=csv.QUOTE_NONE,names=labels)
print(houses.head())

print(houses.shape)

houses.plot(x='SQFT',y='LAST SALE PRICE', kind='scatter')

houses.groupby('SQFT').describe()


#Extract the target values
y= houses['LAST SALE PRICE'].copy()
houses_features =houses.iloc[:,[0,1,2,3,4,5,6,7,8]].copy()

print ("features: \n",houses_features.head())
print("target: \n", y.head())

#Normailze the data
import math
def convert_parking_type(features):
    features['PARKING TYPE'] \
    = features['PARKING TYPE'].map(lambda x : 1 if x.strip().lower() == 'garage' else 0)
    return features
        
houses_features = convert_parking_type(houses_features);
print(houses_features['PARKING TYPE'].head())

def feature_normalize(matrix, mean=None, std=None):
    mean = matrix.mean() if mean is None else mean
    std = matrix.std()  if std is None else std
    matrix = (matrix - mean)/std
    return {'matrix_norm': matrix.fillna(0), 'mean': mean, 'std': std}


results = feature_normalize(houses_features)
houses_norm = results['matrix_norm']
X_mean =results['mean']
X_std = results['std']
                      
results = feature_normalize(y);
y_norm = results['matrix_norm']
y_mean = results['mean']
y_std = results['std']

print ('{0}\n'.format(houses_norm.head()))
print ('{0}\n'.format(y_norm.head()))

scaler = preprocessing.StandardScaler().fit(houses_features.values)
houses_norm = scaler.transform(houses_features)
print("Mean: {0}, STD: {1}".format(scaler.mean_,scaler.std_))
print(houses_norm)


#training our model
def train_model(X,y):
    regressor = LinearRegression(fit_intercept=True)
    regressor.fit(X,y)
    return regressor

regr = train_model(houses_norm, y_norm)
print(regr.coef_)

house_test = pandas.DataFrame({"LIST PRICE" : 749888,
 "BEDS" : 3,
 "BATHS": 2.5,
 "SQFT": 1642,
 "LOT SIZE" : 9876,
 "YEAR BUILT" : 1968,
 "PARKING SPOTS" : 2, 
 "PARKING TYPE" : "Garage",
 "ORIGINAL LIST PRICE": 74988},index=[0])
house_test_converted = convert_parking_type(house_test)
house_test_norm = feature_normalize(house_test_converted, X_mean, X_std)

y_predicted_norm = regr.predict(house_test_norm['matrix_norm']);
y_predicted = y_predicted_norm*y_std + y_mean
print('Predicted Price: {0}'.format(y_predicted[0].round(0)))



print('Normalized Input: \n {0} \n'.format(house_test_norm['matrix_norm']))

print("Mean square error: {0} \n".format(mean_squared_error([y_norm[2]],y_predicted_norm)))
print("R2 Score: {0} \n".format(regr.score(houses_norm, y_norm)))

houses_norm_train,houses_norm_test, y_norm_train, y_norm_test = \
train_test_split(houses_norm,y_norm,test_size=0.2) #<=== 20% of the samples are used for testing.
print(len(houses_norm_train),len(houses_norm_test),len(houses_norm_train)+len(houses_norm_test))


linear_regressor_pipeline = Pipeline([
('normalize', preprocessing.StandardScaler()),
('regressor', LinearRegression())
])
    
scores = cross_val_score(linear_regressor_pipeline,
                         houses_norm_train, #<== training samples
                         y_norm_train,#<== training output labels
                         cv=5,#<=== split data randomly into 5 parts, 4 for training and 1 for scoring
                         scoring='r2', #<== use R^2 score
                         n_jobs=-1)
print(scores,scores.mean(),scores.std())    


labels=["LIST PRICE","BEDS","BATHS","SQFT","LOT SIZE","YEAR BUILT",
        "PARKING SPOTS","PARKING TYPE","ORIGINAL LIST PRICE","LAST SALE PRICE"]
data=pandas.read_csv('./data/redfin_more_data1.csv', quoting=csv.QUOTE_NONE,names=labels)
print(data.head())
data.plot(x='SQFT', y='LAST SALE PRICE', kind='scatter')
data = convert_parking_type(data)
data.interpolate(method='cubic',inplace=True)
data.fillna(0)


y= data['LAST SALE PRICE'].copy()
X =data.iloc[:,[0,1,2,3,4,5,6,7]].copy()

print ("features {0}: \n{1}".format(X.shape, X.head()))
print("target:{0}: \n{1}".format(y.shape, y.head()))

X_train,X_test, y_train, y_test = \
train_test_split(X,y,test_size=0.2,random_state=42) #<=== 20% of the samples are used for testing.
print(len(X_train),len(X_test),len(X_train)+len(X_test))
print(len(y_train) + len(y_test))
print (X_train.shape)
print (y_train.shape)


from sklearn.linear_model import Ridge
from sklearn.metrics import explained_variance_score, r2_score, mean_squared_error, mean_absolute_error
ridge_regressor_pipeline = Pipeline([
('normalize', preprocessing.StandardScaler()),
('regressor',Ridge())
])

scores = cross_val_score(ridge_regressor_pipeline,
                         X_train, #<== training samples
                         y_train,#<== training output labels
                         cv=5,#<=== split data randomly into 5 parts, 4 for training and 1 for scoring
                         scoring='r2', #<== use R^2 score
                         n_jobs=-1)

print(scores,scores.mean(),scores.std())
print("Accuracy: %0.5f (+/- %0.5f)" % (scores.mean(), scores.std() * 2))


ridge_regressor_pipeline.fit(X_train,y_train)
y_predicted = ridge_regressor_pipeline.predict(X_test)
print("Variance Score: %0.05f and R2 Score: %0.5f \n" % (explained_variance_score(y_test,y_predicted), r2_score(y_test,y_predicted)))


def plot_learning_curve(estimator, title, X, y, ylim=None, cv=None,
                        n_jobs=-1, train_sizes=np.linspace(.1, 1.0, 5)):
    """
    Generate a simple plot of the test and traning learning curve.

    Parameters
    ----------
    estimator : object type that implements the "fit" and "predict" methods
        An object of that type which is cloned for each validation.

    title : string
        Title for the chart.

    X : array-like, shape (n_samples, n_features)
        Training vector, where n_samples is the number of samples and
        n_features is the number of features.

    y : array-like, shape (n_samples) or (n_samples, n_features), optional
        Target relative to X for classification or regression;
        None for unsupervised learning.

    ylim : tuple, shape (ymin, ymax), optional
        Defines minimum and maximum yvalues plotted.

    cv : integer, cross-validation generator, optional
        If an integer is passed, it is the number of folds (defaults to 3).
        Specific cross-validation objects can be passed, see
        sklearn.cross_validation module for the list of possible objects

    n_jobs : integer, optional
        Number of jobs to run in parallel (default 1).
    """
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")
    train_sizes, train_scores, test_scores = learning_curve(
        estimator, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label="Cross-validation score")

    plt.legend(loc="best")
    return plt
#time plot_learning_curve(ridge_regressor_pipeline, "accuracy vs. training set size", X_train, y_train, cv=5)

from sklearn.linear_model import Ridge
from sklearn.cross_validation import StratifiedKFold

#load the data
houses=pandas.read_csv('./data/redfin_more_data1.csv', quoting=csv.QUOTE_NONE,names=["LIST PRICE","BEDS","BATHS","SQFT","LOT SIZE","YEAR BUILT",
        "PARKING SPOTS","PARKING TYPE","ORIGINAL LIST PRICE","LAST SALE PRICE"])
houses.interpolate(inplace=True) #replace missing values by interpolating the data in a particular column
y= houses['LAST SALE PRICE'].copy()
houses_features =houses.iloc[:,[0,1,2,3,4,5]].copy()

#split 80% of data to training set and 20% to test set
houses_train,houses_test, y_train, y_test = \
train_test_split(houses_features,y,test_size=0.2) #<=== 20% of the samples are used for testing.

#build the pipeline and use the StandardScalar to normalize the data
ridge_regressor_pipeline = Pipeline([
('normalize', preprocessing.StandardScaler()),
('regressor', Ridge())
])

#set some params for the regressor
param_ridge = [
{'regressor__alpha':[0.01, 0.1, 1], 'regressor__solver':['lsqr'],'regressor__tol': [.99], 'regressor__max_iter': [500] },
{'regressor__alpha':[0.01, 0.1, 1], 'regressor__solver':['cholesky'],'regressor__tol': [.99], 'regressor__max_iter': [500] },
]

#search the best fitting params for our regressor
grid_ridge = GridSearchCV(
    ridge_regressor_pipeline, #pipeline from above
    param_grid=param_ridge, #parameters to tune via cross validation
    refit=True, #fit using all data for the best regressor
    n_jobs=-1,
    scoring='r2',
    cv=StratifiedKFold(y_train,n_folds=5)
)


#time price_predictor = grid_ridge.fit(houses_train,y_train)
print (price_predictor.grid_scores_)


yp = price_predictor.predict(houses_test)
print("Variance: %0.02f and R2 Score: %0.02f \n" % (explained_variance_score(y_test,yp), r2_score(y_test,yp)))

import math
data = {"LIST PRICE":879000,
"BEDS": 3,
"BATHS": 2.5,
"SQFT": 1900,
"LOT SIZE" : 6800,
"YEAR BUILT":2015} 
house_test = pandas.DataFrame(data,index=[0],columns=["LIST PRICE","BEDS","BATHS","SQFT","LOT SIZE","YEAR BUILT"])
#time yp=price_predictor.predict(house_test)
print("Predicted Price: {0}".format(round(yp[0])))




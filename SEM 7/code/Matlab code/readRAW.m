function SaveComponentToCsv
clear all
close all
data = ones(61440,14);
portnum1 = 12;   %COM Port #
comPortName1 = sprintf('COM%d', portnum1);
% Baud rate for use with TG_Connect() and TG_SetBaudrate().
TG_BAUD_57600 =      57600;
% Data format for use with TG_Connect() and TG_SetDataFormat().
TG_STREAM_PACKETS =     0;
% Data type that can be requested from TG_GetValue().
TG_DATA_POOR_SIGNAL =     1;
TG_DATA_ATTENTION =       2;
TG_DATA_MEDITATION =      3;
TG_DATA_RAW =             4;
TG_DATA_DELTA =           5;
TG_DATA_THETA =           6;
TG_DATA_ALPHA1 =          7;
TG_DATA_ALPHA2 =          8;
TG_DATA_BETA1 =           9;
TG_DATA_BETA2 =          10;
TG_DATA_GAMMA1 =         11;
TG_DATA_GAMMA2 =         12;
TG_DATA_BLINK_STRENGTH = 37;
%load thinkgear dll
loadlibrary('thinkgear.dll','thinkgear.h');
fprintf('thinkgear.dll loaded\n');
%get dll version
dllVersion = calllib('Thinkgear', 'TG_GetDriverVersion');
fprintf('ThinkGear DLL version: %d\n', dllVersion );

connectionId1 = calllib('Thinkgear', 'TG_GetNewConnectionId');
if ( connectionId1 < 0 )
  error( sprintf( 'ERROR: TG_GetNewConnectionId() returned %d.\n', connectionId1 ) );
  calllib('Thinkgear', 'TG_FreeConnection', connectionId1 );
end;
% Set/open stream (raw bytes) log file for connection
errCode = calllib('Thinkgear', 'TG_SetStreamLog', connectionId1, 'streamLog.txt' );
if( errCode < 0 )
  error( sprintf( 'ERROR: TG_SetStreamLog() returned %d.\n', errCode ) );
  calllib('Thinkgear', 'TG_FreeConnection', connectionId1 );
end;
% Set/open data (ThinkGear values) log file for connection
errCode = calllib('Thinkgear', 'TG_SetDataLog', connectionId1, 'dataLog.txt' );
if( errCode < 0 )
  error( sprintf( 'ERROR: TG_SetDataLog() returned %d.\n', errCode ) );
  calllib('Thinkgear', 'TG_FreeConnection', connectionId1 );
end;
% Attempt to connect the connection ID handle to serial port "COM3"
errCode = calllib('Thinkgear', 'TG_Connect', connectionId1,comPortName1,TG_BAUD_57600,TG_STREAM_PACKETS );
if ( errCode < 0 )
  error( sprintf( 'ERROR: TG_Connect() returned %d.\n', errCode ) );
  calllib('Thinkgear', 'TG_FreeConnection', connectionId1 );
end
fprintf( 'Connected.  Reading Packets...\n' );

%i = 1;
%j = 1;
%recording data
for i=1:61440   %loop for 120 seconds
  if (calllib('Thinkgear','TG_ReadPackets',connectionId1,1) == 1)   %if a   packet was read...
        for j=1:14         
            if(j == 1)
                data(i,j) = now;
            end
            if(j == 2)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_POOR_SIGNAL) ~= 0)   %if RAW has been updated 
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_POOR_SIGNAL);
                end
            end
            if(j == 3)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_ATTENTION) ~= 0)
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_ATTENTION);
                end
            end
            if(j == 4)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_MEDITATION) ~= 0) 
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_MEDITATION);
                end
            end
            if(j == 5)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_RAW) ~= 0)
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_RAW);
                end
            end
            if(j == 6)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_DELTA) ~= 0)
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_DELTA);
                end
            end
            if(j == 7)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_THETA) ~= 0)
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_THETA);
                end
            end
            if(j == 8)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_ALPHA1) ~= 0)
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_ALPHA1);
                end
            end
            if(j == 9)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_ALPHA2) ~= 0)
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_ALPHA2);
                end
            end
            if(j == 10)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_BETA1) ~= 0)
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_BETA1);
                end
            end
            if(j == 11)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_BETA2) ~= 0)
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_BETA2);
                end
            end
            if(j == 12)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_GAMMA1) ~= 0)
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_GAMMA1);
                end
            end
            if(j == 13)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_GAMMA2) ~= 0)
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_GAMMA2);
                end
            end
            if(j == 14)
                if (calllib('Thinkgear','TG_GetValueStatus',connectionId1,TG_DATA_BLINK_STRENGTH) ~= 0)
                    data(i,j) = calllib('Thinkgear','TG_GetValue',connectionId1,TG_DATA_BLINK_STRENGTH);
                end
            end
        end
    end
end
%save data to csv file

calllib('Thinkgear', 'TG_FreeConnection', connectionId1 );